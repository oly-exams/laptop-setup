#!/bin/bash

set -e

# Update packages
sudo apt update

# Install dependencies
sudo apt install -yy \
    curl htop \
    git python3-pip python3-venv libcups2-dev \
    autossh ansible

# Create directories
mkdir -p $HOME/src

# Print server
cd $HOME/src
if [ -d "print_server" ]; then
    cd $HOME/src/print_server
    git pull
else
    git clone https://gitlab.com/oly-exams/print_server.git
    cd $HOME/src/print_server
    python3 -m venv venv
    ./venv/bin/pip install -r requirements.txt
fi

# Scan uploader
cd $HOME/src
if [ -d "scan_uploader" ]; then
    cd $HOME/src/scan_uploader
    git pull
else
    git clone https://gitlab.com/oly-exams/scan_uploader.git
    cd $HOME/src/scan_uploader
    python3 -m venv venv
    ./venv/bin/pip install -r requirements.txt
fi
