# Laptop setup tool

The following repo contains the helper scripts to bootstrap a new event laptop.

```
wget -q -O - https://gitlab.com/oly-exams/laptop-setup/-/raw/main/setup.sh | bash
```
